package com.example.demo;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class SecurityConfigTest extends TestCase {
	
	 /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public SecurityConfigTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( SecurityConfigTest.class );
    }
    
    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	System.out.println("test");
        assertTrue( true );
    }
    
    //$2a$10$1nuDfCm10l.Kn/.yo16.U.7fRvYa0Wh.yDwuEpWwp635HStHxVxSe
	public void testEncode() {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
		String encodePass = encoder.encode("123");
		System.out.println(encodePass);
//		System.out.println("test");
		assertTrue( true );
	}//
	//$2a$10$dq7.JBeHv7eSzQtoilQNaOIPVKd/xxGmd7z8bN.DzCS2sQhEm9wmu
	public void testDecode() {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
		boolean f = encoder.matches("123","$2a$10$1nuDfCm10l.Kn/.yo16.U.7fRvYa0Wh.yDwuEpWwp635HStHxVxSe");
        System.out.println(f);
        boolean f2 = encoder.matches("123","$2a$10$dq7.JBeHv7eSzQtoilQNaOIPVKd/xxGmd7z8bN.DzCS2sQhEm9wmu");
        System.out.println(f2);
		assertTrue( true );
	}
}
